﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace LibraryAPI.Models
{
    public class LibraryAPIContext : DbContext
    {
        public LibraryAPIContext (DbContextOptions<LibraryAPIContext> options)
            : base(options)
        {
        }

        public DbSet<LibraryAPI.Models.Client> Client { get; set; }
    }
}
