﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryAPI.Models
{
    public class Client
    {
        public int clientId { get; set; }
        public string name { get; set; }
        public string apPaterno { get; set; }
        public string apMaterno { get; set; }
        public string email { get; set; }
        public Nullable<System.DateTime> birthdate { get; set; }
        public string gender { get; set; }
        public Nullable<int> active { get; set; }
        public Nullable<System.DateTime> createdAt { get; set; }
    }
}
